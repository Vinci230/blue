import requests
import execjs
import re
import csv
headers = {
    'Accept': 'application/json, text/plain, */*',
    'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    'Content-Type': 'application/json',
    'Origin': 'http://www.hh1024.com',
    'Pragma': 'no-cache',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'cross-site',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
    'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
}

json_data = execjs.compile(open("hongrendj_log.js").read()).call("main")
print(json_data)

response = requests.post('https://user.hrdjyun.com/wechat/phonePwdLogin', headers=headers, json=json_data)
dict=response.json()
# Note: json_data will not be serialized by requests
# exactly as it was in the original request.
#data = '{"phoneNum":"186236802301","pwd":"bb20bbb46f996ee83923e80941abce7f","t":1689758945579,"tenant":1,"sig":"09bb79e2e66221ea3286bbdf1042df5f"}'
#response = requests.post('https://user.hrdjyun.com/wechat/phonePwdLogin', headers=headers, data=data)
headers1 = {
    'Accept': 'application/json, text/plain, */*',
    'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    'Content-Type': 'application/json;charset=UTF-8',
    'Origin': 'http://www.hh1024.com',
    'Pragma': 'no-cache',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'cross-site',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
    'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
}

json_data2 = execjs.compile(open("hongrendj_sign.js", 'r', encoding='utf-8').read()).call("main", dict['data']['token'])

print(json_data2)
#
response1 = requests.post('https://ucp.hrdjyun.com:60359/api/dy', headers=headers1, json=json_data2)
# print(response1.text)
obj=re.compile('.*?"anchorName":(?P<name>.*?),"avatar".*?"followerCount":(?P<followercount>.*?),"followingCount"',re.S)
data=obj.finditer(response1.text)
f=open("data.csv", 'w', encoding='utf-8')
csvwriter=csv.writer(f)
for i in data:
    #print(i.group('name'))
    #print(i.group("followercount"))
    dic=i.groupdict()
    dic['name']=dic['name'].strip(' "')#strip()函数用于移除字符串中头尾指定的字符，默认为空格或换行符，或其他字符序列
    dic['followercount']=dic['followercount'].strip()
    csvwriter.writerow(dic.values())
f.close()
print('over!')


