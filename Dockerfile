
FROM python:3.9
WORKDIR /app

COPY hongrendj.py /app
COPY hongrendj_log.js /app
COPY hongrendj_sign.js /app
COPY requirements.txt /app
RUN pip install -r requirements.txt
RUN pip install requests
COPY package.json package-lock.json /app/
#RUN npm install
COPY deployment.yaml /app/deployment.yaml
EXPOSE 8080

CMD ["python", "hongrendj.py"]
# 使用Python 3.9作为基础镜像
#FROM node:16-bullseye as modules
#WORKDIR /app
#COPY package.json /app
#COPY package-lock.json /app
#RUN npm install
#
#FROM python:3.9
#WORKDIR /app
#COPY --from=modules /node_modules /app
#COPY hongrendj_log.js \
#hongrendj_sign.js \
#requirements.txt \
#hongrendj.py /app/
#RUN pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
#RUN apt update&&apt install nodejs -y
#
#EXPOSE 8080
#CMD ["python","test_hongrendj.py"]